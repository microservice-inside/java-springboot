FROM openjdk:8-alpine

# INIT ROOT IMAGE -> TODO should be another Docker image

# Maintainer of this Dockerfile
MAINTAINER victor.ladouceur

# Application label
LABEL gen.tech.it.vendor="WEFACTORIT"

# Create default application home and application user
RUN mkdir -p /usr/appl && \
    addgroup wefactor && \
    adduser -S neouser -G wefactor -h /usr/appl

# Switch from root to neo user
USER neouser

# Switch to root application directory
WORKDIR /usr/appl/

# Create neo folder
RUN mkdir neo

# Switch to neo home directory
WORKDIR /usr/appl/neo

# Copy required artefact
ADD target/neo-0.1.0.jar .

EXPOSE 8080

CMD ["java", "-jar", "neo-0.1.0.jar"]