package gen.tech.org;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication(scanBasePackages = {"gen.tech.org"})
@EnableConfigurationProperties
public class NeoApiApplication
{
    public static void main( String[] args )
    {
        SpringApplication.run(NeoApiApplication.class, args);
    }
}
