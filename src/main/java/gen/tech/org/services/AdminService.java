package gen.tech.org.services;

import gen.tech.org.configuration.ApplicationConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminService {

    private final ApplicationConfiguration applicationConfiguration;

    @Autowired
    public AdminService(final ApplicationConfiguration applicationConfiguration){
        this.applicationConfiguration = applicationConfiguration;
    }

    /**
     * Return application status: name, version and its environment
     * @return String formated in JSON format
     */
    public String getStatus(){
        return applicationConfiguration.toString();
    }
}
