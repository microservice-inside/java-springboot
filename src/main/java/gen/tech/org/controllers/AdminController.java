package gen.tech.org.controllers;

import gen.tech.org.services.AdminService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/admin")
public class AdminController {

    @Resource
    private AdminService adminService;

    @GetMapping(value="/status",  produces = "application/json")
    public ResponseEntity<String> getApplicationStatus(){
        try{
            final String applicationStatus =  adminService.getStatus();
            return ResponseEntity.status(HttpStatus.OK).body(applicationStatus);
        }catch (Exception ex){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
        }
    }
}
