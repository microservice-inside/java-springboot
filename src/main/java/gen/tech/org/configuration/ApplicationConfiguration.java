package gen.tech.org.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
public class ApplicationConfiguration {

    @Value("${info.build.name}")
    private String applicationName;

    @Value("${info.build.version}")
    private String applicationVersion;

    @Value(("${info.server.environment}"))
    private String applicationEnvironment;

    @Value("${info.build.timestamp}")
    private String applicationBuild;


    @Override
    public String toString() {
        return "{" +
                "applicationName='" + applicationName + '\'' +
                ", applicationVersion='" + applicationVersion + '\'' +
                ", applicationEnvironment='" + applicationEnvironment + '\'' +
                ", applicationBuild='" + applicationBuild + '\'' +
                '}';
    }
}
